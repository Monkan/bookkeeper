﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Linq;

namespace Bookkeeper
{
		public class BookkeeperManager{
		private static BookkeeperManager instance;
		private SQLiteConnection db;


		public static BookkeeperManager Instance
		{
			get 
			{
				if (instance == null)
				{
					instance = new BookkeeperManager();
				}
				return instance;
			}
		}

		private BookkeeperManager() 
		{
			Console.WriteLine ("BookkeeperManager created");

			string dbpath = System.Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			db = new SQLiteConnection (dbpath + @"\myDatabase.db");

			try
			{
				db.Table<Entry>().Count();		
			}
			catch (SQLiteException e)
			{
				db.CreateTable<Entry>();
				db.CreateTable<Account> ();
				db.CreateTable<TaxRate> ();
				//db.CreateTable<IncomeAccount> ();


				db.Insert (new Account () { Id = 1, Name = "Eget uttag", Number = 1000 });
				db.Insert (new Account () { Id = 2, Name = "Reklam/PR", Number = 1001 });
				db.Insert (new Account () { Id = 3, Name = "Övriga köp", Number = 1002 });
				db.Insert (new Account () { Id = 4, Name = "Kassa", Number = 2003});
				db.Insert (new Account () { Id = 5, Name = "Företagskonto", Number = 2004 });
				db.Insert (new Account () { Id = 6, Name = "Egna insättningar", Number = 2001});
				db.Insert (new Account () { Id = 7, Name = "Försäljning", Number = 3010 });
				db.Insert (new Account () { Id = 7, Name = "Uthyrning", Number = 3093 });


			
				db.Insert (new TaxRate () { Id =1, Tax = 0.06});
				db.Insert (new TaxRate () { Id =2, Tax = 0.12});
				db.Insert (new TaxRate () { Id =3, Tax = 0.25});
				db.Insert (new TaxRate () { Id =4, Tax = 0.20});
			}
		}

		public List<Entry> Entries 
		{
			get 
			{
				return db.Table<Entry>().ToList();
			}
		
		}
		public List<Account> Accounts
		{
			get 
			{ 
				return db.Table <Account>().Where(a => a.Number >= 1000 && a.Number < 2000).ToList();
			}
		}

		public List<Account> IncomeAccounts
		{
			get
			{
				return db.Table<Account> ().Where (a => a.Number >= 2000 && a.Number < 3000).ToList ();
				//return db.Table<Account> ().Take(2).ToList ();
			}
		}

		public List<Account> MoneyAccounts
		{
			get
			{
				return db.Table<Account> ().Where (a => a.Number >= 3000).ToList (); 
			}
		}
				
			
		public List<TaxRate> Taxrates 
		{
			get 
			{
				return db.Table <TaxRate> ().ToList ();
			}
		}

		public void AddEntry(Entry f)
		{
			db.Insert (f);
			System.Console.WriteLine ("Entry created!", f);
		}
    }		
}

