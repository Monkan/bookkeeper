﻿using System;
using SQLite;

namespace Bookkeeper
{
	public class Entry
	{	
		[PrimaryKey, AutoIncrement]
		public int AccountId { get; set; }
		public string Sum { get; set; }
		public int TaxRateId { get; set; }
		public string Description { get; set; }
		public string Date { get; set; }
		public int IncomeAccountId { get; set; }

		public Entry ()
		{
		}
		public override string ToString ()
		{
			return string.Format ("{0}, {1}, {2}", Sum, Description, Date);

		}
	}
}

