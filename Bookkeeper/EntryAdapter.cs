﻿using System;
using Android.App;
using System.Collections.Generic;
using Android.Widget;
using Android.Views;
using System.Linq;

namespace Bookkeeper
{
	public class EntryAdapter : BaseAdapter
	{
		private Activity activity;
		private List<Entry> list; 
		//private BookkeeperManager manager;
		public EntryAdapter (Activity activity, List<Entry> list)
	

		{
			this.activity = activity;
			this.list = list;
			//this.manager = BookkeeperManager.Instance;
		}

		#region implemented abstract members of BaseAdapter

		public override Java.Lang.Object GetItem (int position)
		{
			return null;
		}

		public override long GetItemId (int position)
		{
			return position;
		}

		public override Android.Views.View GetView (int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
		{
			View view = convertView ?? activity.LayoutInflater.Inflate(Resource.Layout.EntryListItem,
				parent, false);

			Entry entry = list [position];
			view.FindViewById<TextView> (Resource.Id.textViewDate).Text = entry.Date;
			view.FindViewById<TextView> (Resource.Id.textViewDescription).Text = entry.Description;
			view.FindViewById<TextView> (Resource.Id.textViewAmount).Text = entry.Sum;

			return view;
		}

		public override int Count {
			get {
				return list.Count;
			}
		}

		#endregion
	}
}

