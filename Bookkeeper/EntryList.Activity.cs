﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Bookkeeper
{
	[Activity (Label = "EntryList")]			
	public class EntryList : Activity
	{
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			SetContentView (Resource.Layout.EntryList);

			BookkeeperManager manager = BookkeeperManager.Instance;
			ListView listView = FindViewById<ListView> (Resource.Id.listView1);
			listView.Adapter = new EntryAdapter (this, manager.Entries);

			// Create your application here
		}
	}
}

