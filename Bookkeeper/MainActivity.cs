﻿using Android.App;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using System.Linq;

namespace Bookkeeper
{
	[Activity (Label = "Bookkeeper", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		private Button newEventButton;
		private Button showAllEventsButton;
		private Button createReportsButton;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			newEventButton = FindViewById<Button> (Resource.Id.newEventButton);
			showAllEventsButton = FindViewById<Button> (Resource.Id.showAllEventsButton);
			createReportsButton = FindViewById<Button> (Resource.Id.createReportsButton);

			newEventButton.Click += delegate {

				StartActivity (typeof(NewEventActivity));
			};

			showAllEventsButton.Click += delegate {

				StartActivity (typeof(EntryList));

			};

			createReportsButton.Click += delegate {

			};
		}

	}
		
}



