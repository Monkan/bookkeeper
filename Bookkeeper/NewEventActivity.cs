﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Bookkeeper
{
	[Activity (Label = "NewEventActivity")]			
	public class NewEventActivity : Activity
	{
		private RadioButton radioButtonIncome;
		private RadioButton radioButtonExpence;
		private Spinner spinnerAccountType;
		private EditText date;
		private EditText description;
		private EditText sum;
		private Button saveEntry;
		private Spinner spinnerName;
		private Spinner spinnerAccountTax;
		private BookkeeperManager manager;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView (Resource.Layout.NewEntry);

			manager = BookkeeperManager.Instance;

			spinnerAccountType = FindViewById<Spinner> (Resource.Id.spinnerAccountType);

			radioButtonIncome = FindViewById<RadioButton> (Resource.Id.radioButtonIncome);
			radioButtonExpence = FindViewById<RadioButton> (Resource.Id.radioButtonExpence);

			radioButtonIncome.Click += UpdateSpinnerAccountType;
			radioButtonExpence.Click += UpdateSpinnerAccountType;

			spinnerAccountTax = FindViewById<Spinner> (Resource.Id.spinnerAccountVat);
			ArrayAdapter<TaxRate> taxAdapter = new ArrayAdapter<TaxRate> (this, Android.Resource.Layout.SimpleSpinnerItem, manager.Taxrates);
			taxAdapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spinnerAccountTax.Adapter = taxAdapter;

			spinnerName = FindViewById<Spinner> (Resource.Id.spinnerName);
			ArrayAdapter<Account> adapter = new ArrayAdapter<Account> (this, Android.Resource.Layout.SimpleSpinnerItem, manager.MoneyAccounts);
			spinnerName.Adapter = adapter;

			date = FindViewById<EditText> (Resource.Id.editTextDate);
			description = FindViewById<EditText> (Resource.Id.editTextDescription);
			sum = FindViewById<EditText> (Resource.Id.editTextAmount);

			saveEntry = FindViewById<Button> (Resource.Id.addEventButton);

			saveEntry.Click += (object sender, EventArgs e) => {
				Entry entry = new Entry();
				entry.Description = description.Text;
				entry.Date = date.Text;
				entry.Sum = sum.Text;
				entry.TaxRateId = ((ArrayAdapter<TaxRate>) spinnerAccountTax.Adapter).GetItem(spinnerAccountTax.SelectedItemPosition).Id;
				manager.AddEntry(entry);

				List<Entry>  entries= manager.Entries;
				foreach(Entry f in entries)
				{
					System.Console.WriteLine ("Entry created!", f);
				}
					
			};
	
		}
		private void UpdateSpinnerAccountType(object sender, EventArgs e)
		{
			if (radioButtonIncome.Checked)
				{
				List<Account> IncomeAccounts = manager.IncomeAccounts.ToList();
				ArrayAdapter<Account> adapter = new ArrayAdapter<Account> (this, Android.Resource.Layout.SimpleSpinnerItem, IncomeAccounts);
				adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
				spinnerAccountType.Adapter = adapter;
				} else 
				{
				List<Account> Accounts = manager.Accounts.ToList();
				ArrayAdapter<Account> adapter = new ArrayAdapter<Account> (this, Android.Resource.Layout.SimpleSpinnerItem, Accounts);
				adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
				spinnerAccountType.Adapter = adapter;
				}
			}
	  }
}

