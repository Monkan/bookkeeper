﻿using System;
using SQLite;

namespace Bookkeeper
{
	public class TaxRate

	{
		[PrimaryKey]
		public double Tax { get; set; }
		public int Id { get; set; }

		public TaxRate ()
		{
		}

		public override string ToString ()
		{
			return string.Format ("{0}", Tax);
		}
	}
	
}

